﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVGSplitterForGTS
{
    public static class LABColor
    {

        public static double ColorDistance(Color c1, Color c2)
        {
            double cl1, ca1, cb1, cl2, ca2, cb2;
            RGBtoCIELAB(c1.R, c1.G, c1.B, out cl1, out ca1, out cb1);
            RGBtoCIELAB(c2.R, c2.G, c2.B, out cl2, out ca2, out cb2);
            return Math.Sqrt((cl1 - cl2) * (cl1 - cl2)
                + (ca1 - ca2) * (ca1 - ca2)
                + (cb1 - cb2) * (cb1 - cb2));
        }

        public static System.Drawing.Color ColorFromLAB(double cL, double ca, double cb)
        {
            double r, g, b;
            CIELABtoRGB(cL, ca, cb, out r, out g, out b);
            System.Drawing.Color color = System.Drawing.Color.FromArgb(255, (int)Math.Min(255, Math.Max(0, r))
                , (int)Math.Min(255, Math.Max(0, g)), (int)Math.Min(255, Math.Max(0, b)));
            return color;
        }

        public static void RGBtoCIELAB(double r,double g,double b,out double cl,out double ca,out double cb)
        {
            double FD = (6.0 / 29) * (6.0 / 29) * (6.0 / 29);
            double FK = (29 / 6.0) * (29 / 6.0) / 3.0;
            double FV =4.0/29;
            double lr = r <= 0.04045d ? r / 12.92d : Math.Pow((r + 0.055d) / 1.055d, 2.4d);
            double lg = g <= 0.04045d ? g / 12.92d : Math.Pow((g + 0.055d) / 1.055d, 2.4d);
            double lb = b <= 0.04045d ? b / 12.92d : Math.Pow((b + 0.055d) / 1.055d, 2.4d);

            double x = 0.4124d * lr + 0.3576d * lg + 0.1805d * lb;
            double y = 0.2126d * lr + 0.7152d * lg + 0.0722d * lb;
            double z = 0.0193d * lr + 0.1192d * lg + 0.9505d * lb;

            double xn = x / 0.95047d;
            double yn = y / 1.00000d;
            double zn = z / 1.08883d;

            double fx = xn > FD ? Math.Pow(xn, 1d / 3d) : FK * xn + FV;
            double fy = yn > FD ? Math.Pow(yn, 1d / 3d) : FK * yn + FV;
            double fz = zn > FD ? Math.Pow(zn, 1d / 3d) : FK * zn + FV;

            cl = 116d * fy - 16d;
            ca = 500d * (fx - fy);
            cb = 200d * (fy - fz);

        }

        public static void CIELABtoRGB( double cl, double ca, double cb, out double r, out double g, out double b)
        {
            double TD = 6.0 / 29;
            double TK = 3.0 * (6.0 / 29) * (6.0 / 29);
            double TV=-16.0/116;
            double yn = (cl + 16d) / 116d;
            double xn = yn + ca / 500d;
            double zn = yn - cb / 200d;

            double fxn = xn > TD ? Math.Pow(xn, 3d) : TK * (xn + TV);
            double fyn = yn > TD ? Math.Pow(yn, 3d) : TK * (yn + TV);
            double fzn = zn > TD ? Math.Pow(zn, 3d) : TK * (zn + TV);

            double x = fxn * 0.95047d;
            double y = fyn * 1.00000d;
            double z = fzn * 1.08883d;

            double lr = 3.24062547732005470d * x - 1.53720797221031910d * y - 0.49862859869824794d * z;
            double lg = -0.96893071472931970d * x + 1.87575606088524200d * y + 0.04151752384295397d * z;
            double lb = 0.05571012044551063d * x - 0.20402105059848677d * y + 1.05699594225438860d * z;

            r = lr <= 0.0031308d ? lr * 12.92d : Math.Pow(lr, 1d / 2.4d) * 1.055d - 0.055d;
            g = lg <= 0.0031308d ? lg * 12.92d : Math.Pow(lg, 1d / 2.4d) * 1.055d - 0.055d;
            b = lb <= 0.0031308d ? lb * 12.92d : Math.Pow(lb, 1d / 2.4d) * 1.055d - 0.055d;
        }
    }
}
