﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVGSplitterForGTS
{
    class ColorLabel
    {
        public ConcurrentBag<int[]> points = new ConcurrentBag<int[]>();
        public double SumL = 0.0;
        public double SumA = 0.0;
        public double SumB = 0.0;
        public double L;
        public double A;
        public double B;
        private double oldd = double.MaxValue;

        public ColorLabel(int r, int g, int b)
        {

            LABColor.RGBtoCIELAB(r, g, b, out L, out A, out B);
        }

        public ColorLabel(double _l, double _a, double _b)
        {

            L = _l;
            A = _a;
            B = _b;
        }

        public double ConvergenceD()
        {
            int count = points.Count;
            if (count == 0)
                return double.NaN;
            return Math.Sqrt((L - SumL / count) * (L - SumL / count) + (A - SumA / count) * (A - SumA / count) + (B - SumB / count) * (B - SumB / count));
        }

        public ColorLabel Refresh()
        {
            ColorLabel ret = null;
            if (oldd < ConvergenceD())
            {
                ret = new ColorLabel(L, A, B);
                oldd = double.MaxValue;
            }
            else
                oldd = ConvergenceD();
            int count = points.Count;
            if (count != 0)
            {
                L = SumL / count;
                A = SumA / count;
                B = SumB / count;
            }
            SumL = 0.0;
            SumA = 0.0;
            SumB = 0.0;
            points = new ConcurrentBag<int[]>();
            return ret;
        }

        public Color GetColor()
        {
            return LABColor.ColorFromLAB(L, A, B);
        }
    }
}
