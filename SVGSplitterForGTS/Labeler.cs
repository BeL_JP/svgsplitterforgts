﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ImageErosionDilation;

namespace SVGSplitterForGTS
{
    class Labeler
    {
        public int status = -1;
        class PixelData { public Color c = Color.Black; public double dist = 0; }
        ConcurrentBag<ColorLabel> colorLabels;
        ColorLabel[,] pixelLabel;
        Bitmap bmp;
        BitmapPlus bmpp;
        int width;
        int height;
        Color TransparentColor;
        /// <summary>
        /// 初期クラス数目標値
        /// </summary>
        int N;
        /// <summary>
        /// クラス距離閾値
        /// 色が近ければクラスの追加を終了する
        /// </summary>
        double cDistTd = 1600.0;
        /// <summary>
        /// ラベル点の最小数
        /// 下回るとクラスは削除される
        /// </summary>
        int MinLabelPoint;
        /// <summary>
        /// 収束判定閾値
        /// </summary>
        double convTd;
        int potTurdSize;
        double potAlphaMax;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_bmp"></param>
        /// <param name="_minLabelPoint">ラベル点の最小数,この数を下回るとクラスは削除される</param>
        /// <param name="_convTd">収束判定閾値</param>
        public Labeler(Bitmap _bmp, double cdtd, int _minLabelPoint, double _convTd, string transparentColor, int _potTurdSize, double _potAlphaMax)
        {
            colorLabels = new ConcurrentBag<ColorLabel>();
            bmp = _bmp;
            pixelLabel = new ColorLabel[bmp.Width, bmp.Height];
            width = bmp.Width;
            height = bmp.Height;
            N = 48;
            cDistTd = cdtd;
            MinLabelPoint = _minLabelPoint;
            convTd = _convTd;
            potAlphaMax = _potAlphaMax;
            potTurdSize = _potTurdSize;
            if (transparentColor != "")
                TransparentColor = ColorTranslator.FromHtml(transparentColor);
            else
                TransparentColor = Color.FromArgb(0, 0, 0, 0);
        }

        public void Labeling(string title, string saveDirectory)
        {
            //k-means++を用いるのでまず初期化
            status = 0;
            //string key = LoadSVGKey(keyFile);
            bmpp = new BitmapPlus(bmp);
            bmpp.BeginAccess();
            ConcurrentBag<Color> skipColor = new ConcurrentBag<Color>();
            //1つ目の色を決める
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    Color px;
                    px = bmpp.GetPixel(i, j);
                    if (px.A > 128 && (TransparentColor.A == 0 || LABColor.ColorDistance(px, TransparentColor) > 6))
                    {
                        skipColor.Add(px);
                        colorLabels.Add(new ColorLabel(px.R, px.G, px.B));
                        break;
                    }
                }
                if (colorLabels.Count != 0)
                    break;
            }

            while (colorLabels.Count < N)
            {
                status = (int)(30 * colorLabels.Count / (double)N);
                PixelData pd = new PixelData();
                Parallel.For(0, height, (j) =>
                  {
                      for (int i = 0; i < width; i++)
                      {
                          Color px;
                          px = bmpp.GetPixel(i, j);
                          if (px.A < 128 || (TransparentColor.A != 0 && LABColor.ColorDistance(px, TransparentColor) < 6))
                              continue;
                          if (skipColor.Contains(px))
                              continue;

                          double mindist = double.MaxValue;
                          foreach (var colorLabel in colorLabels)
                          {
                              var dist = LABColor.ColorDistance(colorLabel.GetColor(), px);
                              if (mindist > dist)
                                  mindist = dist;
                          }
                          lock (pd)
                          {
                              if (pd.dist < mindist)
                              {
                                  pd.dist = mindist;
                                  pd.c = px;
                              }
                          }
                      }
                  });
                if (pd.dist < (cDistTd / 2.0) * (colorLabels.Count / 40.0) * (colorLabels.Count / 40.0) + cDistTd / 2.0)
                    break;
                Console.WriteLine("pd dist:" + pd.dist + "," + ((cDistTd / 2.0) * (colorLabels.Count / 40.0) * (colorLabels.Count / 40.0) + cDistTd / 2.0));
                skipColor.Add(pd.c);
                colorLabels.Add(new ColorLabel(pd.c.R, pd.c.G, pd.c.B));
            }
            N = colorLabels.Count;

            //k-meansクラスタリング開始
            status = 30;
            int noconv = 0;
            while (true)
            {
                Parallel.For(0, height, (j) =>
                {
                    for (int i = 0; i < width; i++)
                    {
                        Color px;
                        px = bmpp.GetPixel(i, j);
                        if (px.A < 128 || (TransparentColor.A != 0 && LABColor.ColorDistance(px, TransparentColor) < 6))
                            continue;
                        ColorLabel matchedLabel = MatchedLabel(px);
                        AddPoint(matchedLabel, i, j);
                    }
                });
                List<ColorLabel> copyLabels = colorLabels.ToList();
                colorLabels = new ConcurrentBag<ColorLabel>();
                bool labelChanged = false;
                foreach (var label in copyLabels)
                {
                    if (label.points.Count > MinLabelPoint)
                    {
                        colorLabels.Add(label);
                    }
                    else
                    {
                        labelChanged = true;
                    }

                }
                if (!labelChanged)
                {
                    //収束チェック
                    noconv = 0;
                    foreach (var label in colorLabels)
                    {
                        double d = label.ConvergenceD();
                        if (!double.IsNaN(d) && d > convTd)
                        {
                            noconv++;
                            break;
                        }
                    }
                    //収束していたら終了
                    if (noconv == 0)
                        break;
                }
                //継続するためラベルの値をリセット
                foreach (var label in colorLabels)
                {
                    label.Refresh();
                }
            }
            bmpp.EndAccess();
            status = 55;
            KeyGenerator(title, saveDirectory);
            WriteGTSSVG(title, saveDirectory);
        }

        void KeyGenerator(string title, string saveDirectory)
        {
            const string formatstrHole = "<rect x=\"0\" y=\"0\" width=\"{0}\" height=\"{1}\" fill=\"#000\" fill-opacity=\"0\" stroke=\"{2}\" stroke-width=\"3\" />";
            SaveSVG(Path.Combine(saveDirectory, title + "_copy_and_replace.svg"), "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" + width + "\" height=\"" + height + "\">" +
                String.Format(formatstrHole, width, height, ColorTranslator.ToHtml(Color.Black)));
        }

        void WriteGTSSVG(string title, string saveDirectory)
        {
            string svgHeader = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" + width + "\" height=\"" + height + "\">";
            string svgPreview = svgHeader;
            ConcurrentBag<string> svgList = new ConcurrentBag<string>();
            ConcurrentBag<string> lineSvgList = new ConcurrentBag<string>();
            Color lineColor = Color.Black;
            double minBrightness = double.MaxValue;
            //線画クラス検索
            foreach (var label in colorLabels)
            {
                double brt = label.GetColor().GetBrightness();
                if (brt < minBrightness)
                {
                    minBrightness = brt;
                    lineColor = label.GetColor();
                }
            }

            status = 60;
            Parallel.ForEach(colorLabels, (label) =>
            {
                int split = 1;
                Color color = label.GetColor();
                //SVGサイズが15k以下になるまで分割
                while (true)
                {
                    List<Bitmap> screenOrigList = new List<Bitmap>();
                    List<string> labelSvgList = new List<string>();
                    for (int i = 0; i < split * split; i++)
                        screenOrigList.Add(new Bitmap(width, height));
                    bool over15k = false;
                    int splitI = 0, splitJ = 0;
                    foreach (var screenOrig in screenOrigList)
                    {
                        BitmapPlus screen = new BitmapPlus(screenOrig);
                        screen.BeginAccess();
                        for (int j = 0; j < height; j++)
                        {
                            for (int i = 0; i < width; i++)
                            {
                                screen.SetPixel(i, j, Color.White);
                            }
                        }
                        foreach (var point in label.points)
                        {
                            if (width * splitI / split - 10 < point[0] && width * (splitI + 1) / split + 10 > point[0] && height * splitJ / split - 10 < point[1] && height * (splitJ + 1) / split + 10 > point[1])
                                screen.SetPixel(point[0], point[1], Color.Black);
                        }
                        screen.EndAccess();
                        string svgtemp = SVGPath(screenOrig, color);
                        var sum = SVGO(svgHeader + svgtemp + "</svg>").Count();
                        if (sum < 15000)
                            labelSvgList.Add(svgtemp);
                        else
                        {
                            over15k = true;
                            split++;
                            break;
                        }
                        splitI++;
                        if (splitI == split)
                        {
                            splitI = 0;
                            splitJ++;
                        }
                    }
                    if (over15k)
                        continue;
                    foreach (var labelSvg in labelSvgList)
                    {
                        (color == lineColor ? lineSvgList : svgList).Add(labelSvg);
                    }
                    break;
                }
            });
            status = 90;
            var svgListOut = svgList.ToList();
            var lineSvgListOut = lineSvgList.ToList();
            svgListOut.Sort(new Comparison<string>(Compare));
            lineSvgListOut.Sort(new Comparison<string>(Compare));
            int outCnt = 0;
            for (int i = 0; i < svgListOut.Count; i++)
            {
                string temp = svgListOut[i];
                for (i++; i < svgListOut.Count; i++)
                {
                    if (SVGO(svgHeader + temp + svgListOut[i] + "</svg>").Count() < 15000)
                    {
                        temp += svgListOut[i];
                    }
                    else
                    {
                        i--;
                        break;
                    }
                }
                svgPreview += temp;
                SaveSVG(Path.Combine(saveDirectory, title + " " + outCnt++ + ".svg"), svgHeader + temp);
            }
            for (int i = 0; i < lineSvgListOut.Count; i++)
            {
                string temp = lineSvgListOut[i];
                for (i++; i < lineSvgListOut.Count; i++)
                {
                    if (SVGO(svgHeader + temp + lineSvgListOut[i] + "</svg>").Count() < 15000)
                    {
                        temp += lineSvgListOut[i];
                    }
                    else
                    {
                        i--;
                        break;
                    }
                }
                svgPreview += temp;
                SaveSVG(Path.Combine(saveDirectory, title + " " + outCnt++ + ".svg"), svgHeader + temp);
            }

            SaveSVG(Path.Combine("temp\\prev.svg"), svgPreview);
            status = 100;
        }

        public static void SVGPathSpec(string testBmp,string destSvg)
        {
            var testimg = new Bitmap(testBmp);
            string svgHeader = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" + testimg.Width + "\" height=\"" + testimg.Height + "\">";
            SaveSVGRaw(destSvg, svgHeader+SVGPath(testimg, Color.Green));
        }

        static string SVGOut(Color color,string path)
        {
            return "<path fill-rule=\"nonzero\" fill=\"" + ColorTranslator.ToHtml(color) + "\" stroke=\"" + ColorTranslator.ToHtml(color) + "\" stroke-width=\"3px\" d=\"" + path + "\"/>";
        }

        static string SVGPath(Bitmap monobmp, Color color)
        {
            BitmapPlus bmpp = new BitmapPlus(monobmp);
            bmpp.BeginAccess();
            List<List<Point>> borderPointAll = new List<List<Point>>();
            Func<int, int, bool> bitExist = (x, y) => {
                if (x < 0 || x >= monobmp.Width || y < 0 || y >= monobmp.Height)
                    return false;
                var c = bmpp.GetPixel(x, y);
                return c.ToArgb() == Color.Black.ToArgb() || c.ToArgb() == Color.Red.ToArgb();
            };

            Func<int, int, bool> checkBorder = (x, y) =>
            {
                if (bitExist(x, y))
                {
                    if (bitExist(x, y - 1) || bitExist(x - 1, y) || bitExist(x + 1, y) || bitExist(x, y + 1))
                        return !bitExist(x - 1, y - 1) || !bitExist(x, y - 1) || !bitExist(x + 1, y - 1) ||
                               !bitExist(x - 1, y) || !bitExist(x + 1, y) ||
                               !bitExist(x - 1, y + 1) || !bitExist(x, y + 1) || !bitExist(x + 1, y + 1);
                }
                return false;
            };

            Func<int, int, bool> isBorder = (x, y) =>
            {
                if (x < 0 || x >= monobmp.Width || y < 0 || y >= monobmp.Height)
                    return false;
                var c = bmpp.GetPixel(x, y);
                return c.ToArgb() == Color.Red.ToArgb();
            };

            int rx = 0;
            int ry = -1;

            int r2x = 0;
            int r2y = 0;
            Func<List<Point> ,Point> nextBorder = (pl) =>
            {
                var last = pl.Last();
                Point newPoint = new Point(-1,-1);
                int i = 0;
                Func<bool> rotate = () =>
                {
                    if (rx == 1 && ry == 0)
                    {
                        rx = 0;
                        ry = 1;
                    }
                    else if (rx == 0 && ry == 1)
                    {
                        rx = -1;
                        ry = 0;
                    }
                    else if (rx == -1 && ry == 0)
                    {
                        rx = 0;
                        ry = -1;
                    }
                    else if (rx == 0 && ry == -1)
                    {
                        rx = 1;
                        ry = 0;
                    }
                    return true;
                };

                Func<bool> rotateSub = () =>
                {
                    if (rx == 1 && ry == 0)
                    {
                        r2x = 1;
                        r2y = 1;
                    }
                    else if (rx == 0 && ry == 1)
                    {
                        r2x = -1;
                        r2y = 1;
                    }
                    else if (rx == -1 && ry == 0)
                    {
                        r2x = -1;
                        r2y = -1;
                    }
                    else if (rx == 0 && ry == -1)
                    {
                        r2x = 1;
                        r2y = -1;
                    }
                    return true;
                };

                if (pl.Count == 1)
                {
                    rx = 0;
                    ry = -1;
                    bool flag = false;
                    for (; i < 8; i++)
                    {
                        rotateSub();
                        if (flag && checkBorder(last.X + rx, last.Y + ry))
                        {
                            newPoint = new Point(last.X + rx, last.Y + ry);
                            rotate();
                            rotate();
                            rotate();
                            break;
                        }
                        if (!bitExist(last.X + rx, last.Y + ry) || !bitExist(last.X + r2x, last.Y + r2y))
                        {
                            flag = true;
                        }
                        rotate();
                    }
                }
                else
                {
                    for (; i < 8; i++)
                    {
                        if (checkBorder(last.X + rx, last.Y + ry))
                        {
                            newPoint = new Point(last.X + rx, last.Y + ry);
                            rotate();
                            rotate();
                            rotate();
                            break;
                        }
                        rotate();
                    }
                }

                if (newPoint.X == pl.First().X && newPoint.Y == pl.First().Y)
                    newPoint = new Point(-1, -1);
                if(i == 8)
                    newPoint = new Point(-1, -1);
                if (newPoint.X != -1 && newPoint.Y != -1)
                {
                    pl.Add(newPoint);
                }
                return newPoint;
             };

            Func<int, int, int> loopI = (x,length) =>
            {
                while (x < 0)
                    x += length;
                return x % length;
            };

            List<Point> currentPoints = null;
            for (int j = 0;j < monobmp.Height; j++)
            {
                for(int i = 0; i < monobmp.Width; i++)
                {
                    if (checkBorder(i, j) && !isBorder(i, j))
                    {
                        bmpp.SetPixel(i, j, Color.Red);
                        Point nextPoint = new Point(i, j);
                        currentPoints = new List<Point>();
                        currentPoints.Add(nextPoint);
                        while (true)
                        {
                            nextPoint = nextBorder(currentPoints);
                            if(nextPoint.X == -1 && nextPoint.Y == -1)
                                break;
                            currentPoints.Add(nextPoint);
                            bmpp.SetPixel(nextPoint.X, nextPoint.Y, Color.Red);
                        }
                        if(currentPoints.Count > 1)
                            borderPointAll.Add(currentPoints);
                    }
                }
            }
            string rsvg = "";
            //境界それぞれ処理
            foreach(var borderPoints in borderPointAll)
            {
                List<Point> dborderPoints = new List<Point>(); //微分
                string svg = "";
                for (int i = 0; i < borderPoints.Count; i++)
                {
                    var dx = borderPoints[loopI(i + 2, borderPoints.Count)].X - borderPoints[loopI(i - 2, borderPoints.Count)].X;
                    var dy = borderPoints[loopI(i + 2, borderPoints.Count)].Y - borderPoints[loopI(i - 2, borderPoints.Count)].Y;
                    dborderPoints.Add(new Point(dx, dy));
                }
                int p0x = borderPoints[0].X;
                int p0y = borderPoints[0].Y;
                int dp0x = dborderPoints[0].X;
                int dp0y = dborderPoints[0].Y;
                svg += "M" + p0x + "," + p0y + " ";

                for (int i = 1; i < borderPoints.Count; i+=5)
                {
                    var cp1x = borderPoints[loopI(i - 5, borderPoints.Count)].X + dborderPoints[loopI(i - 5, borderPoints.Count)].X / (3.0 * borderPoints.Count);
                    var cp1y = borderPoints[loopI(i - 5, borderPoints.Count)].Y + dborderPoints[loopI(i - 5, borderPoints.Count)].Y / (3.0 * borderPoints.Count);
                    var cp2x = borderPoints[loopI(i, borderPoints.Count)].X + dborderPoints[loopI(i, borderPoints.Count)].X / (3.0 * borderPoints.Count);
                    var cp2y = borderPoints[loopI(i, borderPoints.Count)].Y + dborderPoints[loopI(i, borderPoints.Count)].Y / (3.0 * borderPoints.Count);
                    svg += string.Format("C {0:F1} {1:F1}, {2:F1} {3:F1}, {4:F1} {5:F1} ", cp1x, cp1y, cp2x, cp2y, borderPoints[loopI(i, borderPoints.Count)].X, borderPoints[loopI(i, borderPoints.Count)].Y);
                }
                rsvg += svg + "Z ";
            }
            bmpp.EndAccess();
            return SVGOut(color, rsvg);
        }

        static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X-p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        static string SVGO(string input)
        {
            string guid = Guid.NewGuid().ToString("N");
            StreamWriter sw = new StreamWriter("temp\\"+ guid+".svg", false, Encoding.UTF8);
            sw.Write(input);
            sw.Close();
            return Form1.CMDExec("svgo temp\\"+ guid+".svg -o -");
        }

        static int Compare(string a,string b)
        {
            return b.Count() - a.Count();
        }
        

        string LoadSVGKey(string filePath)
        {
            StreamReader sr = new StreamReader(filePath, Encoding.UTF8);
            string svg = sr.ReadToEnd();
            svg = svg.Substring(svg.IndexOf('>') + 1);
            svg = svg.Substring(0,svg.LastIndexOf('<'));
            return Regex.Replace(svg, @"fill="".*?""", "fill=\"#FF0000\"");
        }

        static void SaveSVG(string filePath, string svgText)
        {
            StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8);
            sw.Write(SVGO(svgText + "</svg>"));
            sw.Close();
        }

        static void SaveSVGRaw(string filePath, string svgText)
        {
            StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8);
            sw.Write(svgText + "</svg>");
            sw.Close();
        }

        ColorLabel MatchedLabel(Color color)
        {
            ColorLabel label = null;
            double mindist = double.MaxValue;
            foreach (var colorLabel in colorLabels)
            {
                double dist = LABColor.ColorDistance(colorLabel.GetColor(), color);
                if (mindist > dist)
                {
                    label=colorLabel;
                    mindist = dist;
                }
            }

            return label;
        }

        void AddPoint(ColorLabel newLabel, int x, int y)
        {
            pixelLabel[x, y] = newLabel;
            pixelLabel[x, y].points.Add(new int[2] { x, y });
            Color c = bmpp.GetPixel(x, y);
            double cl, ca, cb;
            LABColor.RGBtoCIELAB(c.R, c.G, c.B, out cl, out ca, out cb);
            pixelLabel[x, y].SumL += cl;
            pixelLabel[x, y].SumA += ca;
            pixelLabel[x, y].SumB += cb;
        }
    }
}
