﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVGSplitterForGTS
{
    public partial class FormPreview : Form
    {
        public FormPreview()
        {
            InitializeComponent();
            ImageMagick.MagickImage mimg = new ImageMagick.MagickImage("temp\\prev.svg");
            mimg.Scale(new ImageMagick.Percentage(80));
            pictureBox1.Image = mimg.ToBitmap();
        }
    }
}
