﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageMagick;

namespace SVGSplitterForGTS
{
    public partial class Form1 : Form
    {
        int revision = 302;
        double cdtd = 2000;
        int minLabelPoint = 1000;
        double convTd = 4.0;
        int turdSize=4;
        double alphamax=0.5;
        Labeler labeler=null;
        Bitmap img;
        FormPreview fPreview;
        bool highlight = false;
        public Form1()
        {
            InitializeComponent();
            Text += string.Format("{0:0.00}", revision / 100.0);
            numericUpDown3.DecimalPlaces = 1;
            numericUpDown6.DecimalPlaces = 2;
            numericUpDown3.Value = (decimal)convTd;
            numericUpDown4.Value = minLabelPoint;
            numericUpDown5.Value = turdSize;
            numericUpDown6.Value = (decimal)alphamax;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //環境変数を設定
            System.Environment.SetEnvironmentVariable("Path", System.Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.User)+";"+System.Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.Machine));
        }

        private void Restart()
        {
            System.Diagnostics.Process.Start("elbot.exe");
            Application.Exit();
        }
        public static string CMDExec(string cmd)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            //ComSpec(cmd.exe)のパスを取得して、FileNameプロパティに指定
            p.StartInfo.FileName = System.Environment.GetEnvironmentVariable("ComSpec");
            //出力を読み取れるようにする
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardInput = false;
            //ウィンドウを表示しないようにする
            p.StartInfo.CreateNoWindow = true;
            //コマンドラインを指定（"/c"は実行後閉じるために必要）
            p.StartInfo.Arguments = @"/c "+cmd;

            //起動
            p.Start();

            //出力を読み取る
            string results = p.StandardOutput.ReadToEnd();

            //プロセス終了まで待機する
            //WaitForExitはReadToEndの後である必要がある
            //(親プロセス、子プロセスでブロック防止のため)
            p.WaitForExit();
            p.Close();
            return results;
        }

        void ReadIMG()
        {
            try
            {
                MagickImage img2 = new MagickImage(textBox1.Text);
                img2.Scale(new Percentage(img2.Width > img2.Height ? 1000d * 100d / img2.Width : 1000d * 100d / img2.Height));
                img = img2.ToBitmap();
                numericUpDown4.Value = img.Width * img.Height / 1500;
                numericUpDown3.Value = (decimal)(img.Width * img.Height / 300000d);
                numericUpDown5.Value = (img.Width * img.Height) / 80000;
            }
            catch (Exception ex)
            {
                errorDialog(2, ex.Message);
            }
        }

        void IMGSplit()
        {
            labeler = new Labeler(img, cdtd,minLabelPoint,convTd, textBox5.Text,turdSize,alphamax);
            if (!Directory.Exists("temp"))
                Directory.CreateDirectory("temp");
            else
            {
                foreach (var file in Directory.GetFiles("temp"))
                    File.Delete(file);
            }
            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                labeler.Labeling(textBox4.Text==""?"notitle":textBox4.Text,textBox2.Text);
            })).Start();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
            }catch(Exception ex)
            {
                errorDialog(1, ex.Message);
            }
        }

        private void errorDialog(int p,string errortxt)
        {
            MessageBox.Show("以下の内容を開発者まで報告お願いしますm(_ _)m\n"+"エラーポイント:"+p+"\n"+errortxt,
                "エラー",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog()== DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox1.Text = openFileDialog1.FileName;
            if (textBox1.Text != "")
            {
                ReadIMG();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
                return;
            button3.Visible = false;
            progressBar1.Visible = true;
            IMGSplit();
        }
        
        private void Form1_Shown(object sender, EventArgs e)
        {
            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Node.jsのインストールを確認中...";
                });
                if (CMDExec("npm -h") != "")
                {
                    Invoke((MethodInvoker)delegate
                    {
                        label1.Text = "svgoのインストールを確認中...";
                    });
                    CMDExec("npm install -g svgo");
                    Invoke((MethodInvoker)delegate
                    {
                        panel1.Visible = false;
                        Labeler.SVGPathSpec(@"C:\Users\tss_a\Pictures\test.bmp", @"C:\Users\tss_a\Pictures\dest.svg");
                    });
                    return;
                }
                DialogResult result = MessageBox.Show("Node.jsをインストールする必要があります。\nOKをクリックしてインストール後，手動で再起動してください。",
                "警告",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    System.Diagnostics.Process.Start("https://nodejs.org/ja/");
                }
                Application.Exit();
            })).Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (labeler != null && labeler.status != -1)
            {
                progressBar1.Value = labeler.status;
                if (labeler.status == 100)
                {
                    button3.Visible = true;
                    progressBar1.Visible = false;
                    labeler = null;
                    fPreview = new FormPreview();
                    fPreview.ShowDialog();
                }
            }
            if (textBox1.Text == "" || textBox2.Text == "")
                button3.BackColor = Color.SlateGray;
            else
                button3.BackColor = Color.LimeGreen;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            convTd = (double)numericUpDown3.Value;
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            minLabelPoint = (int)numericUpDown4.Value;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox4.Text = Path.GetFileNameWithoutExtension(textBox1.Text);
            button1.BackColor = Color.MediumSeaGreen;
        }

        private void label9_DoubleClick(object sender, EventArgs e)
        {
            groupBox4.Visible = true;
            groupBox2.Visible = false;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
                cdtd = 2200;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
                cdtd = 2000;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
                cdtd = 1800;
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (highlight = !highlight)
            {
                button1.BackColor = Color.MediumSeaGreen;
                button2.BackColor = Color.MediumSeaGreen;
            }
            else
            {
                if (textBox1.Text == "")
                    button1.BackColor = Color.Lime;
                if (textBox2.Text == "")
                    button2.BackColor = Color.Lime;
            }
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            button2.BackColor = Color.MediumSeaGreen;
        }
    }
}
